[![PHP from Packagist](https://img.shields.io/packagist/php-v/manyprintsolutions/status)](http://www.php.net)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/4313110fdc5c4fa5a462ecd95be2a21f)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/manyprintsolutions-status-php&amp;utm_campaign=Badge_Grade)
[![Total Downloads](https://poser.pugx.org/manyprintsolutions/status/downloads)](https://packagist.org/packages/manyprintsolutions/status)
[![License](https://poser.pugx.org/manyprintsolutions/status/license)](https://packagist.org/packages/manyprintsolutions/status)
[![composer.lock](https://poser.pugx.org/manyprintsolutions/status/composerlock)](https://packagist.org/packages/manyprintsolutions/status)

# ManyPrintSolutions Status

Status Enums for ManyPrint Solutions. 

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/manyprintsolutions/status). Add it to your project by running `$ composer require manyprintsolutions/status` and `$ composer update`.

## Usage

A Job may have 4 different states: 

*   `PENDING` The job is waiting to get handeled
*   `IN_PROGRESS` The job is about to get handeled
*   `FINISHED` The handling is finished
*   `ABORTED` The handling was aborted

## Help 

If you have questions feel free to contact us under `manyprintsolutions@bitandblack.com`.