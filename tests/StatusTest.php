<?php

namespace ManyPrintSolutions\Status\Tests;

use ManyPrintSolutions\Status\StatusEnum;
use PHPUnit\Framework\TestCase;

/**
 * Class StatusTest
 * 
 * @package ManyPrintSolutions\Status\Tests
 */
class StatusTest extends TestCase
{
    public function testNumbers()
    {
        $this->assertEquals(
            StatusEnum::PENDING()->getValue(),
            'PENDING'
        );

        $this->assertEquals(
            StatusEnum::IN_PROGRESS()->getValue(),
            'IN_PROGRESS'
        );

        $this->assertEquals(
            StatusEnum::FINISHED()->getValue(),
            'FINISHED'
        );

        $this->assertEquals(
            StatusEnum::ABORTED()->getValue(),
            'ABORTED'
        );
    }
}