<?php

namespace ManyPrintSolutions\Status;

use MyCLabs\Enum\Enum;

/**
 * Class StatusEnum
 * 
 * @package ManyPrintSolutions\Status
 * @method static StatusEnum PENDING()
 * @method static StatusEnum IN_PROGRESS()
 * @method static StatusEnum FINISHED()
 * @method static StatusEnum ABORTED()
 */
final class StatusEnum extends Enum
{
    const PENDING = 'PENDING';
    const IN_PROGRESS = 'IN_PROGRESS';
    const FINISHED = 'FINISHED';
    const ABORTED = 'ABORTED';
}